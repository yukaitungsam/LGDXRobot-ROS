#include "coreRos.h"

CoreRos::CoreRos(ros::NodeHandle n, QObject *parent) : QObject(parent)
{
    this->n = n;
}

CoreRos::~CoreRos()
{
    ROS_INFO("ROS shutdown");
    ros::shutdown();
}

void CoreRos::nodeLogCallback(const rosgraph_msgs::Log::ConstPtr& log)
{
    QString nodeName = QString::fromLatin1(log->name.c_str());
    QString msg = QString::fromLatin1(log->msg.c_str());
    QString myLog = nodeName + "," + msg;
    emit nodeLogArrived(myLog);
}

void CoreRos::realsenseD400CallBack(const sensor_msgs::Image::ConstPtr& img)
{
    emit realsenseD400Arrived();
}

void CoreRos::realsenseT265CallBack(const nav_msgs::Odometry::ConstPtr& odom)
{
    float x = odom->pose.pose.position.x;
    float y = odom->pose.pose.position.y;
    emit realsenseT265Arrived(x, y);
}

void CoreRos::stm32CallBack(const std_msgs::String::ConstPtr& str)
{
    QString string = QString::fromLatin1(str->data.c_str());
    emit stm32Arrived(string);
}

void CoreRos::runRos()
{
    ros::Subscriber sub = n.subscribe("/rosout_agg", 100, &CoreRos::nodeLogCallback, this);
    ros::Subscriber sub2 = n.subscribe("/d400/aligned_depth_to_color/image_raw", 10, &CoreRos::realsenseD400CallBack, this);
    ros::Subscriber sub3 = n.subscribe("/t265/odom/sample", 10, &CoreRos::realsenseT265CallBack, this);
    ros::Subscriber sub4 = n.subscribe("/lgdx_robot/stm_out", 10, &CoreRos::stm32CallBack, this);
    ros::spin();
}