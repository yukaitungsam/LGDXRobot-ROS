#include <QProcess>
#include <QThread>
#include <QSerialPort>
#include <QSerialPortInfo>

#include <cstdlib>
#include <memory>

#include <signal.h>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"

#include "std_srvs/Empty.h"
#include "lgdx_robot/StmServo.h"
#include "lgdx_robot/StmPid.h"

// Const
const quint16 kRequireVendorId = 1155; // Only accept STMicroelectronics boards

QSerialPort stm32Serial;
QString stm32Port = "";
QString stm32Buffer = "";

ros::NodeHandle *n;
ros::Publisher stm32Pub;
ros::Subscriber sub;

using namespace std;

/*
 * Serial Port functions
 */

void searchSTM32()
{
    stm32Port = "";
    while(stm32Port.isEmpty())
    {
        const auto infos = QSerialPortInfo::availablePorts();
        for (const QSerialPortInfo &info : infos)
        {
            if(info.vendorIdentifier() == kRequireVendorId && info.portName().contains("ACM"))
            {
                stm32Port = info.portName();
                ROS_INFO("A STM32 is found on port: %s", stm32Port.toStdString().c_str());
                return;
            }
        }
        ROS_ERROR("STM32 board is not found! Try again in 5 seconds");
        QThread::sleep(5);
    }
}

void connectSTM32()
{
    bool connected = false;
    stm32Serial.setPortName(stm32Port);
    stm32Serial.setBaudRate(QSerialPort::Baud115200);
    stm32Serial.setDataBits(QSerialPort::Data8);
    stm32Serial.setParity(QSerialPort::NoParity);
    stm32Serial.setStopBits(QSerialPort::OneStop);
    stm32Serial.setFlowControl(QSerialPort::NoFlowControl);
    while(!connected)
    {
        if (stm32Serial.open(QIODevice::ReadWrite))
        {
            ROS_INFO("Connected STM32");
            connected = true;
            break;
        }
        else
        {
            ROS_ERROR("Cannot connect to STM32: %s, retry in 5 seconds", stm32Serial.errorString().toStdString().c_str());
            QThread::sleep(5);
        }
    }
}

void disconnectSTM32()
{
    if (stm32Serial.isOpen())
        stm32Serial.close();
    ROS_INFO("Disconnected STM32");
}

void readSTM32()
{
    const QByteArray data = stm32Serial.readAll();
    stm32Buffer += QString::fromStdString(data.data());
    QStringList bufferSplit = stm32Buffer.split("#");
    if(bufferSplit.size() > 1)
    {
        //ROS_INFO("%s", bufferSplit.at(0).toStdString().c_str());
        std_msgs::String msg;
        msg.data = bufferSplit.at(0).toStdString();
        stm32Pub.publish(msg);
        stm32Buffer = bufferSplit.at(1);
    }

}

void writeSTM32(QString command)
{
    if(stm32Serial.isWritable())
    {
        stm32Serial.write(command.toStdString().c_str());
        stm32Serial.waitForBytesWritten();
    }
    else
    {
        ROS_ERROR("Unable to write STM32");
    }
}

/*
 * Robot control
 */
void ik(float vx, float vy, float w)
{
    QString cmd = "K," + QString::number(-vx) + "," + QString::number(vy) + "," + QString::number(-w) + "\r\n";
    writeSTM32(cmd);
}

void servo(int node, float angle)
{
    QString cmd = "S," + QString::number(node) + "," + QString::number(angle) + "\r\n";
    writeSTM32(cmd);
}

void pid(int node, float kp, float ki, float kd)
{
    QString cmd = "P," + QString::number(node) + "," + QString::number(kp) + "," + QString::number(ki) + "," + QString::number(kd) + "\r\n";
    writeSTM32(cmd);
}

/*
 * Handle the topic /cmd_vel
 */
void cmdVelCallback(const geometry_msgs::Twist::ConstPtr& twist)
{
    float x = twist->linear.x;
    float y = twist->linear.y;
    float w = twist->angular.z;
    ROS_INFO("Receive  IK (%f, %f, %f)", x, y, w);
    if(!stm32Serial.isOpen())
    {
        ROS_WARN("The STM is not ready to write");
        return;
    }
    ik(x, y, w);
}

/*
 * Handle Service
 */
bool servoService(lgdx_robot::StmServo::Request  &req, lgdx_robot::StmServo::Response &res)
{
    if(!stm32Serial.isOpen())
    {
        ROS_WARN("The STM is not ready to write");
        return false;
    }

    // Validate
    int node = (int)req.node;
    float angle = (float)req.angle;
    ROS_INFO("Receive servo (%d, %f)", node, angle);
    if(node < 0 && node > 3)
    {
        res.ok = false;
        return false;
    }
    if(angle < 0 && angle > 180)
    {
        res.ok = false;
        return false;
    }
    res.ok = true;

    servo(node, angle);
    return true;
}

bool pidService(lgdx_robot::StmPid::Request  &req, lgdx_robot::StmPid::Response &res)
{
    if(!stm32Serial.isOpen())
    {
        ROS_WARN("The STM is not ready to write");
        return false;
    }

    // Validate
    int node = (bool)req.node;
    float kp = (float)req.kp;
    float ki = (float)req.ki;
    float kd = (float)req.kd;
    ROS_INFO("Receive PID (%f, %f, %f)", kp, ki, kd);
    if(node < 0 && node > 3)
    {
        res.ok = false;
        return false;
    }
    res.ok = true;
    pid(node, kp, ki, kd);
    return true;
}

bool resetSub(std_srvs::Empty::Request  &req, std_srvs::Empty::Response &res)
{
    if(!sub.getTopic().empty())
            sub.shutdown();
    sub = n->subscribe("/cmd_vel", 100, cmdVelCallback);
}

/*
 * Handle the SIGINT (CTRL+C)
 */
void sigintHandler(int sig)
{
    ROS_INFO("System is terminating");
    disconnectSTM32();
    ros::shutdown();
    exit(0);
}

int main(int argc, char **argv)
{
    // Initialize ROS
    ros::init(argc, argv, "lgdx_robot_stm_bridge");
    n = new ros::NodeHandle("~");
    signal(SIGINT, sigintHandler);
    ROS_INFO("STM Bridge node is running");

    stm32Pub = n->advertise<std_msgs::String>("/lgdx_robot/stm_out", 100);
    ros::ServiceServer service1 = n->advertiseService("/lgdx_robot/stm_servo", servoService);
    ros::ServiceServer service2 = n->advertiseService("/lgdx_robot/stm_pid", pidService);
    ros::ServiceServer service3 = n->advertiseService("/lgdx_robot/reset_sub", resetSub);
    sub = n->subscribe("/cmd_vel", 100, cmdVelCallback);

    searchSTM32();
    connectSTM32();

    while (ros::ok())
    {
        if(stm32Serial.waitForReadyRead())
        {
            readSTM32();
        }
        else
        {
            ROS_ERROR("STM32 connection lost, attempting to reconnect.");
            disconnectSTM32();
            searchSTM32();
            connectSTM32();
        }

        ros::spinOnce();
    }
    return 0;
}
