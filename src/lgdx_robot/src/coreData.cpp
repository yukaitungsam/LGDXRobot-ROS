#include "coreData.h"

CoreData::CoreData()
{
    connect(&realsenseD400Timer, &QTimer::timeout, [=]() 
    {
        setRealsenseD400Ready(false);
    });
    connect(&realsenseT265Timer, &QTimer::timeout, [=]() 
    {
        setRealsenseT265Ready(false);
    });
    connect(&stm32Timer, &QTimer::timeout, [=]() 
    {
        setstm32Ready(false);
    });
}

void CoreData::setRealsenseD400Ready(bool b)
{
    if(realsenseD400Ready && b)
    {
        realsenseD400Timer.start(kTimeOutMs);
        return;
    }
    realsenseD400Ready = b; 
    if(realsenseD400Ready)
    {
        ROS_INFO("Realsense D400 Ready");
        realsenseD400Timer.start(kTimeOutMs);
    }
    else
    {
        ROS_INFO("Realsense D400 Disconnected");
        realsenseD400Timer.stop();
    }
    emit changed(CoreDataType::RealsenseD400Ready);
}

void CoreData::setRealsenseT265Ready(bool b)
{
    if(realsenseT265Ready && b)
    {
        realsenseT265Timer.start(kTimeOutMs);
        return;
    }
    realsenseT265Ready = b; 
    if(realsenseT265Ready)
    {
        ROS_INFO("Realsense T265 Ready");
        realsenseT265Timer.start(kTimeOutMs);
    }
    else
    {
        ROS_INFO("Realsense T265 Disconnected");
        realsenseT265Timer.stop();
    }
    emit changed(CoreDataType::RealsenseT265Ready);
}

void CoreData::setstm32Ready(bool b)
{
    if(stm32Ready && b)
    {
        stm32Timer.start(kTimeOutMs);
        return;
    }
    stm32Ready = b; 
    if(stm32Ready)
    {
        ROS_INFO("STM32 Ready");
        stm32Timer.start(kTimeOutMs);
    }
    else
    {
        ROS_INFO("STM32 Disconnected");
        stm32Timer.stop();
    }
    emit changed(CoreDataType::Stm32Ready);
}

QString CoreData::robotStateStatusStr()
{
    QString str = "hw,robot," + QString::number(robotState);
    return str;
}

QString CoreData::d400StatusStr()
{
    QString str = "hw,d400," + QString::number(realsenseD400Ready);
    return str;
}

QString CoreData::t265StatusStr()
{
    QString str = "hw,t265," + QString::number(realsenseT265Ready);
    return str;
}

QString CoreData::stm32StatusStr()
{
    QString str = "hw,mcu," + QString::number(stm32Ready);
    return str;
}

QString CoreData::getHardwareStatus()
{
    QString str = "";
    str += robotStateStatusStr() + "\n";
    str += d400StatusStr() + "\n";
    str += t265StatusStr() + "\n";
    str += stm32StatusStr();
    return str;
}