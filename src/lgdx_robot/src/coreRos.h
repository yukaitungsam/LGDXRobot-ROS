#ifndef COREROS_H
#define COREROS_H

#include "ros/ros.h"
#include "rosgraph_msgs/Log.h"
#include "sensor_msgs/Image.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/String.h"

#include <QObject>
#include <QString>
#include <QCoreApplication>

class CoreRos : public QObject
{
    Q_OBJECT
private:
    ros::NodeHandle n;

    void nodeLogCallback(const rosgraph_msgs::Log::ConstPtr& log);
    void realsenseD400CallBack(const sensor_msgs::Image::ConstPtr& img);
    void realsenseT265CallBack(const nav_msgs::Odometry::ConstPtr& odom);
    void stm32CallBack(const std_msgs::String::ConstPtr& str);

public:
    explicit CoreRos(ros::NodeHandle n, QObject *parent = nullptr);
    ~CoreRos();

public slots:
    void runRos();

signals:
    void nodeLogArrived(QString log);
    void realsenseD400Arrived();
    void realsenseT265Arrived(int x, int y);
    void stm32Arrived(QString data);
};

#endif // COREROS_H