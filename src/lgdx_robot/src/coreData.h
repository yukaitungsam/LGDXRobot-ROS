#ifndef COREDATA_H
#define COREDATA_H

#include <QObject>
#include <QHostAddress>
#include <QTimer>
#include <QString>
#include "ros/ros.h"

/*
 *  ENUM
 */
class ROSNodeClass
{
    Q_GADGET
public:
    enum Value {
        RealsenseT265A,
        RealsenseT265B,
        RealsenseD400A,
        RealsenseD400B,
        TF_Publish_1,
        TF_Publish_2,
        TF_Publish_3,
        TF_WEB,
        AI,
        STMBridge,
        Video,
        Websocket,
        Webapi,
        // Dont move
        DYNAMIC_START,
        // Dynamic
        RtabmapSlam,
        RtabmapLoc,
        RtabmapCloud,
        Movebase,
        Explorer,
        PathPlanner,
        // Size
        SIZE
    };
    Q_ENUM(Value)

private:
    explicit ROSNodeClass();
};
using ROSNode = ROSNodeClass::Value;

class RobotStateClass
{
    Q_GADGET
public:
    enum Value {
        NotInit,
        Idle,
        AutoExplore,
        ManualExplore,
        AutoCollection,
        ManualAll,
        ManualP2P,
        Stopping,
    };
    Q_ENUM(Value)

private:
    explicit RobotStateClass();
};
using RobotState = RobotStateClass::Value;

class CoreDataTypeClass
{
    Q_GADGET
public:
    enum Value {
        RobotState,
        RealsenseD400Ready,
        RealsenseT265Ready,
        Stm32Ready,

    };
    Q_ENUM(Value)

private:
    explicit CoreDataTypeClass();
};
using CoreDataType = CoreDataTypeClass::Value;

/*
 *  Class
 */
class CoreData : public QObject
{
    Q_OBJECT

private:
    // Running
    QHostAddress pcAddress;
    RobotState robotState = RobotState::Idle;
    // Other device status
    const int kTimeOutMs = 1000;
    bool realsenseD400Ready = false;
    QTimer realsenseD400Timer;
    bool realsenseT265Ready = false;
    QTimer realsenseT265Timer;
    bool stm32Ready = false;
    QTimer stm32Timer;
public:
    CoreData();
    void setPcAddress(QHostAddress a){pcAddress = a;}
    QHostAddress getPcAddress(){return pcAddress;}
    void setRobotState(RobotState s){robotState = s; emit changed(CoreDataType::RobotState);}
    RobotState getRobotState(){return robotState;}
    void setRealsenseD400Ready(bool b);
    bool getRealsenseD400Ready(){return realsenseD400Ready;}
    void setRealsenseT265Ready(bool b);
    bool getRealsenseT265Ready(){return realsenseT265Ready;}
    void setstm32Ready(bool b);
    bool getstm32Ready(){return stm32Ready;}
    
    // Respond str
    QString robotStateStatusStr();
    QString d400StatusStr();
    QString t265StatusStr();
    QString stm32StatusStr();
    QString getHardwareStatus();
signals:
    void changed(CoreDataType d);
};

#endif // COREDATA_H