#include <signal.h>
#include <jetson-inference/detectNet.h>
#include <jetson-utils/cudaMappedMemory.h>
#include <jetson-utils/videoOutput.h>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"
#include "sensor_msgs/point_cloud2_iterator.h"

#include "lgdx_robot/AiNodeSwitch.h"

#include <QHash>
#include <QDebug>
#include <cmath>
#include <string>

// Const
size_t kW = 320;
size_t kH = 240;
// Reference
uchar3* cudaImage;
detectNet* aiNet;
ros::NodeHandle *nodeRef;
videoOutput* output;
// Other
ros::Subscriber subscriber;
ros::Publisher aiPub;

/*
 * Handle the SIGINT (CTRL+C)
 */
void sigintHandler(int sig)
{
    ROS_INFO("System is terminating");
    if(!subscriber.getTopic().empty())
        subscriber.shutdown();
    if(aiNet != nullptr)
        SAFE_DELETE(aiNet);
    if(output != nullptr)
        SAFE_DELETE(output);
    ros::shutdown();
}

/*
 * Handle ROS topic
 */
void pointCloud2CallBack(const sensor_msgs::PointCloud2ConstPtr& cloud)
{
    // Get RGB data from point cloud, upload to GPU
    sensor_msgs::PointCloud2ConstIterator<uint8_t> iter_r(*cloud.get(), "r");
    sensor_msgs::PointCloud2ConstIterator<uint8_t> iter_g(*cloud.get(), "g");
    sensor_msgs::PointCloud2ConstIterator<uint8_t> iter_b(*cloud.get(), "b");
    const uint32_t imgWidth  = kW;
    const uint32_t imgHeight = kH;
    const uint32_t imgPixels = imgWidth * imgHeight;
    uint8_t* cpuPtr = (uint8_t*)cudaImage;
    for(uint32_t i = 0; i < imgPixels; i++)
    {
        *cpuPtr = *iter_r;
        cpuPtr++;
        *cpuPtr = *iter_g;
        cpuPtr++;
        *cpuPtr = *iter_b;
        cpuPtr++;
        ++iter_r;
        ++iter_g;
        ++iter_b;
    }
    // Detect objects in the frame
    detectNet::Detection* detections = NULL;
    const int numDetections = aiNet->Detect(cudaImage, kW, kH, &detections, detectNet::OVERLAY_LABEL | detectNet::OVERLAY_BOX | detectNet::OVERLAY_CONFIDENCE);

    /*
     * Book : 84
     * Cup: 47, Bottle: 44
     *
     */
    int reportDistance = 51;
    double reportAngle = 0.0;
    int rubbishType = 0;

    if( numDetections > 0 )
    {
        //ROS_INFO("%d objects detected\n", numDetections);
        for(int n = 0; n < numDetections; n++)
        {
            //ROS_INFO("Detected obj %d  class #%u (%s)  confidence=%f", n, detections[n].ClassID, aiNet->GetClassDesc(detections[n].ClassID), detections[n].Confidence);
            //ROS_INFO("Bounding box %d  (%f, %f)  (%f, %f)  w=%f  h=%f", n, detections[n].Left, detections[n].Top, detections[n].Right, detections[n].Bottom, detections[n].Width(), detections[n].Height());

            // Obtain the distance and angle
            int startCol = ceil(detections[n].Left);
            int startRow = ceil(detections[n].Top);
            int width = int(detections[n].Width());
            int height = int(detections[n].Height());
            QHash<int, int> zStat;
            QHash<int, int> xStat;
            sensor_msgs::PointCloud2ConstIterator<float> iter_z(*cloud.get(), "z");
            sensor_msgs::PointCloud2ConstIterator<float> iter_x(*cloud.get(), "x");
            iter_z += int(startRow * kW);
            iter_x += int(startRow * kW);
            for(int row = startRow; row < (startRow + height); row++)
            {
                iter_z += startCol;
                iter_x += startCol;
                for(int col = startCol; col < (startCol + width); col++)
                {
                    int zDistance = round(*iter_z * 100);
                    int xDistance = round(*iter_x * 100);
                    // put in stat
                    if(zStat.contains(zDistance))
                    {
                        zStat[zDistance] = zStat[zDistance] + 1;
                    }
                    else
                    {
                        zStat[zDistance] = 1;
                    }
                    if(xStat.contains(xDistance))
                    {
                        xStat[xDistance] = xStat[xDistance] + 1;
                    }
                    else
                    {
                        xStat[xDistance] = 1;
                    }
                    ++iter_z;
                    ++iter_x;
                }
                iter_z += int(kW - width - startCol);
                iter_x += int(kW - width - startCol);
            }
            //qDebug() << zStat;
            //qDebug() <<xStat;
            QHash<int, int>::const_iterator i;
            long long zAverage = 0, zCount = 0, zSum = 0;
            for (i = zStat.constBegin(); i != zStat.constEnd(); ++i)
            {
                zSum += i.key() * i.value();
                zCount += i.value();
            }
            zAverage = round(zSum / zCount);
            long long xAverage = 0, xCount = 0, xSum = 0;
            for (i = xStat.constBegin(); i != xStat.constEnd(); ++i)
            {
                xSum += i.key() * i.value();
                xCount += i.value();
            }
            xAverage = round(xSum / xCount);
            //qDebug() << xAverage << zAverage;
            int realDistance = round(sqrt(zAverage*zAverage + xAverage*xAverage));
            double angle = tan(double(xAverage)/double(zAverage));

            // Filter
            if(detections[n].ClassID != 84 && detections[n].ClassID != 47 && detections[n].ClassID != 44 && detections[n].ClassID != 9)
                continue;
            if(realDistance > 50)
                continue;
            if(realDistance > reportDistance)
                continue;
            reportDistance = realDistance;
            reportAngle = angle;

            // Sort
            if(detections[n].ClassID == 84)
                rubbishType = 0;
            if(detections[n].ClassID == 47 || detections[n].ClassID == 44)
                rubbishType = 1;
            if(detections[n].ClassID == 9)
                rubbishType = 2;

            std_msgs::String msg;
            // Type, x, y
            std::string str = std::to_string(rubbishType) + "," + std::to_string(zAverage) + "," +  std::to_string(xAverage);
            msg.data = str;
            aiPub.publish(msg);

            ROS_INFO("---");
            ROS_INFO("Detected obj %d  class #%u (%s)  confidence=%f", n, detections[n].ClassID, aiNet->GetClassDesc(detections[n].ClassID), detections[n].Confidence);
            ROS_INFO("Distance %d cm, angle %f rad, type: %d", reportDistance, reportAngle, rubbishType);
        }
    }
/*
    // render outputs
    if(output != NULL)
    {
        output->Render(cudaImage, kW, kH);

        // update the status bar
        char str[256];
        sprintf(str, "TensorRT %i.%i.%i | %s | Network %.0f FPS", NV_TENSORRT_MAJOR, NV_TENSORRT_MINOR, NV_TENSORRT_PATCH, precisionTypeToStr(aiNet->GetPrecision()), aiNet->GetNetworkFPS());
        output->SetStatus(str);
    }
*/
}

/*
 * Handle Service
 */
bool switchAi(lgdx_robot::AiNodeSwitch::Request  &req, lgdx_robot::AiNodeSwitch::Response &res)
{
    if((bool)req.enable)
    {
        ROS_DEBUG("Start AI");
        subscriber = nodeRef->subscribe("/voxel_cloud", 15, pointCloud2CallBack);
    }
    else
    {
        ROS_DEBUG("Pause AI");
        if(!subscriber.getTopic().empty())
            subscriber.shutdown();
    }
    res.curStatus = req.enable;
    return true;
}

int main(int argc, char **argv)
{
    // Initialize ROS
    ROS_INFO("Welcome to AI Node");
    ros::init(argc, argv, "lgdx_robot_ai");
    ros::NodeHandle n("~");
    nodeRef = &n;
    signal(SIGINT, sigintHandler);

/*
    ROS_INFO("Initializing display window");
    output = videoOutput::Create("display://0");
    if(!output)
        ROS_WARN("Failed to create output stream");
*/
    // Initialize AI
    // AI model
    ROS_INFO("Initializing AI model");
    aiNet = detectNet::Create(detectNet::SSD_MOBILENET_V2);
    if(!aiNet)
    {
        ROS_ERROR("Failed to load detectNet model");
        return 0;
    }
    ROS_INFO("Allocating CUDA resources");
    // CUDA image
    if(!cudaAllocMapped(&cudaImage, kW, kH))
    {
        ROS_ERROR("Failed to allocate CUDA resources");
        return 0;
    }
    aiPub = n.advertise<std_msgs::String>("/lgdx_robot/ai_out", 100);
    ros::ServiceServer service = n.advertiseService("enable", switchAi);
    ROS_INFO("Ready to run");
    //subscriber = nodeRef->subscribe("/voxel_cloud", 15, pointCloud2CallBack);
    ros::spin();

    return 0;
}
