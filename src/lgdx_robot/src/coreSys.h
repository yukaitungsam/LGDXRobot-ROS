#ifndef CORESYS_H
#define CORESYS_H

#include <QProcess>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include <QCoreApplication>
#include <QTcpSocket>
#include <QTcpServer>
#include <QThread>
#include <QSocketNotifier> 

#include <cstdlib>
#include <vector>
#include <memory>

#include "ros/ros.h"
#include <ros/package.h>
#include "coreRos.h"
#include "geometry_msgs/Twist.h"
#include "lgdx_robot/AiNodeSwitch.h"
#include "lgdx_robot/StmServo.h"
#include "coreData.h"

/*
 *  Class
 */
class CoreSys : public QObject
{
    Q_OBJECT

private:
    // Constant
    const int kTermNodeTimeOut = 10;
    const int kStmDelayTotal = 25;
    const int kMyPort = 34777;
    const int kPCPort = 34689;
    int stmDelayCounter = 0;

    // Store the nodes
    std::vector<std::unique_ptr<QProcess>> rosNodeList;

    // TCP
    std::unique_ptr<QTcpServer> tcpInServer; 
    QTcpSocket *tcpInClient; // Auto delete
    std::unique_ptr<QTcpSocket> tcpOutClient;

    // Status
    CoreData robotData;
    bool connectedPC = false;

    // ROS thread
    std::unique_ptr<CoreRos> ros;
    QThread rosThread;

    // ROS
    ros::Publisher manualCmdVel;
    ros::ServiceClient aiClient;
    ros::ServiceClient stmServoClient;
    ros::ServiceClient stmResetClient;

    // Node
    std::unique_ptr<QProcess> createNode(ROSNode node);
    void startAutoExplore();
    void startManualExplore();
    void startCollection();
    void startManualAll();
    void startManualP2P();
    void stopNodes(bool stopAll = false);
    
    // TCP
    void tcpInNewConnection();
    void tcpInReceive();
    void tcpSend(QString str);

private slots:
    void logArrived(QString log);
    void realsenseD400Arrived();
    void realsenseT265Arrived(int x, int y);
    void stm32Arrived(QString data);

public:
    explicit CoreSys(ros::NodeHandle n, QObject *parent = nullptr);
    ~CoreSys();

public slots:
    void dataChanged(CoreDataType d);
    void terminateThread(int unused = 0);

signals:
};

#endif // CORESYS_H
