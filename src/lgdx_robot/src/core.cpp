#include <QCoreApplication>
#include <QString>
#include <QStringList>
#include <QProcess>
#include "coreSys.h"
#include "sigwatch.h" // Credit

int main(int argc, char *argv[])
{
    // Initialize ROS
    ros::init(argc, argv, "lgdx_robot_core");
    ros::NodeHandle n("~");
    ROS_INFO("Welcome to lgdx_robot for Jetson");
    
    // Parameter
    ROS_INFO("Loading parameters");
    QString program1 = "rosparam";
    QStringList arguments1;
    arguments1 << "delete" << "/";
    QProcess *process1 = new QProcess();
    process1->start(program1, arguments1);
    process1->waitForFinished();

    QString program = "roslaunch";
    QStringList arguments;
    arguments << "lgdx_robot" << "loadParam.launch";
    QProcess *process = new QProcess();
    process->start(program, arguments);
    process->waitForFinished();

    QCoreApplication a(argc, argv);
    CoreSys sys(n);
    UnixSignalWatcher sigwatch;
    sigwatch.watchForSignal(SIGINT);
    sigwatch.watchForSignal(SIGTERM);
    QObject::connect(&sigwatch, &UnixSignalWatcher::unixSignal, &sys, &CoreSys::terminateThread);
    return a.exec();
}
