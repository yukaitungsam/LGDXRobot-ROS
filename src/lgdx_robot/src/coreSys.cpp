#include "coreSys.h"

CoreSys::CoreSys(ros::NodeHandle n, QObject *parent) : QObject(parent)
{
    // Initalize TCP
    tcpInServer = std::make_unique<QTcpServer>(this);
    if(tcpInServer->listen(QHostAddress::Any, kMyPort))
    {
        ROS_INFO("TCP server is starting, the TCP port is: %d", kMyPort);
        connect(tcpInServer.get(), &QTcpServer::newConnection, this, &CoreSys::tcpInNewConnection);
    }
    else
    {
        ROS_FATAL("TCP server is unable to start");
        exit(0);
    }
    tcpOutClient = std::make_unique<QTcpSocket>(this);
    QObject::connect(
        tcpOutClient.get(), &QTcpSocket::connected,
        [=]() {
        qDebug() << "TCP Connected";
        if(!connectedPC)
        {
            connectedPC = true;
            // Send my data
            tcpSend(robotData.getHardwareStatus());
        }
    });
    QObject::connect(
        tcpOutClient.get(), &QTcpSocket::disconnected,
        [=]() {
        qDebug() << "TCP Disonnected";
        connectedPC = false;
    });
    /*
    QObject::connect(
        tcpOutClient.get(), &QTcpSocket::errorOccurred,
        [=]() {
        qDebug() << "TCP Error:" << tcpOutClient->errorString();
    });
    */ 
    // Signal
    QObject::connect(&robotData, &CoreData::changed, this, &CoreSys::dataChanged);

    rosNodeList.resize(int(ROSNodeClass::SIZE));
    // Start Hardware
    rosNodeList[ROSNodeClass::RealsenseT265A] = createNode(ROSNode::RealsenseT265A);
    rosNodeList[ROSNodeClass::RealsenseT265B] = createNode(ROSNode::RealsenseT265B);
    rosNodeList[ROSNodeClass::RealsenseD400A] = createNode(ROSNode::RealsenseD400A);
    rosNodeList[ROSNodeClass::RealsenseD400B] = createNode(ROSNode::RealsenseD400B);
    rosNodeList[ROSNodeClass::TF_Publish_1] = createNode(ROSNode::TF_Publish_1);
    rosNodeList[ROSNodeClass::TF_Publish_2] = createNode(ROSNode::TF_Publish_2);
    rosNodeList[ROSNodeClass::TF_Publish_3] = createNode(ROSNode::TF_Publish_3);
    rosNodeList[ROSNodeClass::TF_WEB] = createNode(ROSNode::TF_WEB);
    // Start Other node
    rosNodeList[ROSNodeClass::AI] = createNode(ROSNode::AI);
    rosNodeList[ROSNodeClass::STMBridge] = createNode(ROSNode::STMBridge);
    rosNodeList[ROSNodeClass::Video] = createNode(ROSNode::Video);
    rosNodeList[ROSNodeClass::Websocket] = createNode(ROSNode::Websocket);
    rosNodeList[ROSNodeClass::Webapi] = createNode(ROSNode::Webapi);

    ros = std::make_unique<CoreRos>(n);
    ros->moveToThread(&rosThread);
    connect(ros.get(), &CoreRos::nodeLogArrived, this, &CoreSys::logArrived);
    connect(ros.get(), &CoreRos::realsenseD400Arrived, this, &CoreSys::realsenseD400Arrived);
    connect(ros.get(), &CoreRos::realsenseT265Arrived, this, &CoreSys::realsenseT265Arrived);
    connect(ros.get(), &CoreRos::stm32Arrived, this, &CoreSys::stm32Arrived);
    connect(&rosThread, &QThread::started, ros.get(), &CoreRos::runRos);
    rosThread.start();

    aiClient = n.serviceClient<lgdx_robot::AiNodeSwitch>("/lgdx_robot_ai/enable");
    stmServoClient = n.serviceClient<lgdx_robot::StmServo>("/lgdx_robot/stm_servo");
    manualCmdVel = n.advertise<geometry_msgs::Twist>("/cmd_vel", 100);
}

CoreSys::~CoreSys()
{

}

std::unique_ptr<QProcess> CoreSys::createNode(ROSNode node)
{
    auto process = std::make_unique<QProcess>();
    QString program = "";
    QStringList arguments;
    QString nodeName = "";
    switch (node)
    {
        case ROSNode::RealsenseT265A:
            program = "rosrun";
            arguments << "nodelet" << "nodelet" << "manager" << "__ns:=t265";
            nodeName = "Realsense";
            break;
        case ROSNode::RealsenseT265B:
            program = "rosrun";
            arguments << "nodelet" << "nodelet" << "load" << "realsense2_camera/RealSenseNodeFactory" << "manager" << "__ns:=t265" << "_device_type:=t265" << "_publish_tf:=false" << "_enable_fisheye1:=false" << "_enable_fisheye2:=false" << "_topic_odom_in:=odom_in" << "_enable_pose:=true" << "_base_frame_id:=t265_link" << "_pose_frame_id:=t265_pose_frame" << "_odom_frame_id:=t265_odom_frame";
            nodeName = "Realsense";
            break;
        case ROSNode::RealsenseD400A:
            program = "rosrun";
            arguments << "nodelet" << "nodelet" << "manager" << "__ns:=d400";
            nodeName = "Realsense";
            break;
        case ROSNode::RealsenseD400B:
            program = "rosrun";
            arguments << "nodelet" << "nodelet" << "load" << "realsense2_camera/RealSenseNodeFactory" << "manager" << "__ns:=d400" << "_device_type:=d4.5" << "_align_depth:=true" << "_color_width:=640" << "_color_height:=480" << "_depth_width:=640" << "_depth_height:=480" << "_color_fps:=30" << "_depth_fps:=30" << "_enable_infra1:=false" << "_enable_infra2:=false" << "_base_frame_id:=d400_link" << "_aligned_depth_to_color_frame_id:=d400_aligned_depth_to_color_frame" << "_color_frame_id:=d400_color_frame" << "_color_optical_frame_id:=d400_color_optical_frame" << "_depth_frame_id:=d400_depth_frame" << "_depth_optical_frame_id:=d400_depth_optical_frame";
            nodeName = "Realsense";
            break;
        case ROSNode::TF_Publish_1:
            // pose_to_base
            program = "rosrun";
            arguments << "tf" << "static_transform_publisher" << "0.13" << "0" << "0.06" << "0" << "0" << "0" << "t265_pose_frame" << "base_link" << "100";
            nodeName = "TF Publisher 1";
            break;
        case ROSNode::TF_Publish_2:
            // base_to_t265
            program = "rosrun";
            arguments << "tf" << "static_transform_publisher" << "0" << "-0.07" << "0.13" << "0" << "0" << "0" << "base_link" << "t265_link" << "100";
            nodeName = "TF Publisher 2";
            break;
        case ROSNode::TF_Publish_3:
            // t265_to_d400
            program = "rosrun";
            arguments << "tf" << "static_transform_publisher" << "-0.0225" << "0.03" << "-0.01" << "0" << "0.122173048" << "0" << "t265_link" << "d400_link" << "100";
            nodeName = "TF Publisher 3";
            break;
        case ROSNode::TF_WEB:
            program = "rosrun";
            arguments << "tf2_web_republisher" << "tf2_web_republisher";
            nodeName = "TF Web";
            break;
        case ROSNode::AI:
            program = "rosrun";
            arguments << "lgdx_robot" << "ai";
            nodeName = "AI";
            break;
        case ROSNode::STMBridge:
            program = "rosrun";
            arguments << "lgdx_robot" << "stm_bridge";
            nodeName = "STMBridge";
            break;
        case ROSNode::Video:
            program = "rosrun";
            arguments << "web_video_server" << "web_video_server";
            nodeName = "Video";
            break;
        case ROSNode::Websocket:
            program = "rosrun";
            arguments << "rosbridge_server" << "rosbridge_websocket";
            nodeName = "Websocket";
            break;
        case ROSNode::Webapi:
            program = "rosrun";
            arguments << "rosapi" << "rosapi_node";
            nodeName = "Webapi";
            break;
        case ROSNode::RtabmapSlam:
            program = "roslaunch";
            arguments << "lgdx_robot" << "rtabmap_slam.launch";
            nodeName = "RtabmapSlam";
            break;
        case ROSNode::RtabmapLoc:
            program = "roslaunch";
            arguments << "lgdx_robot" << "rtabmap_loc.launch";
            nodeName = "RtabmapLoc";
            break;
        case ROSNode::RtabmapCloud:
            program = "rosrun";
            arguments << "nodelet" << "nodelet" << "standalone" << "rtabmap_ros/point_cloud_xyzrgb" << "rgb/image:=/d400/color/image_raw" << "depth/image:=/d400/aligned_depth_to_color/image_raw" << "rgb/camera_info:=/d400/color/camera_info" << "cloud:=voxel_cloud" << "_decimation:=2" << "_voxel_size:=0.0" << "_approx_sync:=true";
            nodeName = "RtabmapCloud";
            break;
        case ROSNode::Movebase:
            program = "rosrun";
            arguments << "move_base" << "move_base" << "__name:=move_base";
            nodeName = "Movebase";
            break;
        case ROSNode::Explorer:
            program = "rosrun";
            arguments << "lgdx_robot_explore" << "explore";
            nodeName = "Explorer";
            break;
        case ROSNode::PathPlanner:
		    program = "rosrun";
		    arguments << "lgdx_robot" << "auto";
		    nodeName = "Path Planner";
            break;
        default:
            ROS_WARN("The process is empty");
            break;
    }
    process->start(program, arguments);
    ROS_INFO("%s node started, PID: %d", nodeName.toStdString().c_str(), int(process->processId()));
    return process;
}

void CoreSys::startAutoExplore()
{
    ROS_INFO("Switching to Auto Explore");
    rosNodeList[ROSNodeClass::RtabmapSlam] = createNode(ROSNode::RtabmapSlam);
    rosNodeList[ROSNodeClass::RtabmapCloud] = createNode(ROSNode::RtabmapCloud);
    rosNodeList[ROSNodeClass::Movebase] = createNode(ROSNode::Movebase);
    rosNodeList[ROSNodeClass::Explorer] = createNode(ROSNode::Explorer); 
    robotData.setRobotState(RobotStateClass::AutoExplore);
}

void CoreSys::startManualExplore()
{
    ROS_INFO("Switching to Manual Explore");
    rosNodeList[ROSNodeClass::RtabmapSlam] = createNode(ROSNode::RtabmapSlam);
    robotData.setRobotState(RobotStateClass::ManualExplore);
}

void CoreSys::startCollection()
{
    ROS_INFO("Switching to Collection");
    rosNodeList[ROSNodeClass::RtabmapLoc] = createNode(ROSNode::RtabmapLoc);
    rosNodeList[ROSNodeClass::Movebase] = createNode(ROSNode::Movebase);
    rosNodeList[ROSNodeClass::RtabmapCloud] = createNode(ROSNode::RtabmapCloud);
    lgdx_robot::AiNodeSwitch aiSwitch;
    aiSwitch.request.enable = true;
    aiClient.call(aiSwitch);
    rosNodeList[ROSNodeClass::PathPlanner] = createNode(ROSNode::PathPlanner);
    robotData.setRobotState(RobotStateClass::AutoCollection);
}

void CoreSys::startManualAll()
{
    ROS_INFO("Switching to Manual All");
    robotData.setRobotState(RobotStateClass::ManualAll);
}

void CoreSys::startManualP2P()
{
    ROS_INFO("Switching to Manual P2P");
    rosNodeList[ROSNodeClass::RtabmapLoc] = createNode(ROSNode::RtabmapLoc);
    rosNodeList[ROSNodeClass::Movebase] = createNode(ROSNode::Movebase);
    // Core accept command
    robotData.setRobotState(RobotStateClass::ManualP2P);
}

void CoreSys::stopNodes(bool stopAll)
{
    ROS_INFO("Stopping operation");
    robotData.setRobotState(RobotStateClass::Stopping);

    // Stop wheel
    geometry_msgs::Twist twist;
    twist.linear.x = 0;
    twist.linear.y = 0;
    twist.angular.z = 0;
    manualCmdVel.publish(twist);
    
    // Stop AI first
    lgdx_robot::AiNodeSwitch aiSwitch;
    aiSwitch.request.enable = false;
    aiClient.call(aiSwitch);

    int startNode = 0;
    if(!stopAll)
        startNode = int(ROSNodeClass::DYNAMIC_START);

    // Send SIGINT to all node
    for(int i = startNode; i < int(ROSNodeClass::SIZE); i++)
    {
        if(rosNodeList.at(i)) // Process exist
        {
            int pid = rosNodeList.at(i)->processId();
            if(pid == 0)
                continue;
            ROS_INFO("Sending SIGINT to PID: %d, node = %d", pid, i);
            QString killStr = "pkill -SIGINT " + QString::number(pid);
            std::system(killStr.toStdString().c_str());
            // Kill More
            if(i == int(ROSNode::RtabmapSlam) || i == int(ROSNode::RtabmapLoc))
            {
                ROS_INFO("Terminaling Rtabmap");
                std::system("pkill -SIGINT rtabmap");
            }
            /*
            else if(i == int(ROSNode::Movebase))
            {
                ROS_INFO("Terminaling move_base");
                std::system("pkill -SIGINT move_base");
            }
            else if(i == int(ROSNode::Explorer))
            {
                ROS_INFO("Terminaling move_explorer");
                std::system("pkill -SIGINT move_base");
                std::system("pkill -SIGINT explore");
            }
            */
        }
    }

    // Wait until timeout
    int startTime = QDateTime::currentSecsSinceEpoch();
    while(1)
    {
        int stopCount = 0;
        for(int i = 0; i < int(ROSNodeClass::SIZE); i++)
        {
            if(!rosNodeList.at(i)) // Process notexist
            {
                stopCount++;
            }
            else // Process exist
            {
                if(rosNodeList.at(i)->atEnd()) // Process is not running
                {
                    stopCount++;
                }
                else if(rosNodeList.at(i)->processId() == 0) // Process is not running
                {
                    stopCount++;
                }
                else // Process is running
                {
                    int currentTime = QDateTime::currentSecsSinceEpoch();
                    if(currentTime - startTime >= kTermNodeTimeOut)
                    {
                        // Kill the process if timeout
                        int pid = rosNodeList.at(i)->processId();
                        ROS_INFO("Node PID: %d timeout, kill", pid);
                        rosNodeList.at(i)->kill();
                        rosNodeList.at(i)->waitForFinished();
                        stopCount++;
                    }
                }
            }
        }

        // Break if all node is terminated
        if(stopAll)
        {
            if(stopCount >= int(ROSNodeClass::SIZE))
            break;
        }
        else
        {
            if(stopCount >= (int(ROSNodeClass::SIZE) - int(ROSNodeClass::DYNAMIC_START)))
            break;
        }
        
    }

    // Final cleaning, make sure all process are killed
    for(int i = startNode; i < int(ROSNodeClass::SIZE); i++)
    {
        if(rosNodeList.at(i))
        {
            int pid = rosNodeList.at(i)->processId();
            rosNodeList.at(i)->kill();
            rosNodeList.at(i)->waitForFinished();
        }
    }

    ROS_INFO("Robot Idle");
    robotData.setRobotState(RobotStateClass::Idle);
}

void CoreSys::tcpInNewConnection()
{
    tcpInClient = tcpInServer->nextPendingConnection();
    ROS_INFO("A new TCP connection form %s", tcpInClient->peerAddress().toString().toStdString().c_str());
    connect(tcpInClient, &QTcpSocket::readyRead, this, &CoreSys::tcpInReceive);
    connect(tcpInClient, &QTcpSocket::disconnected, tcpInClient, &QTcpSocket::deleteLater);
}

void CoreSys::tcpInReceive()
{
    QByteArray datas = tcpInClient->readAll();
    QString dataStr = QString::fromLocal8Bit(datas);
    ROS_INFO("The command is %s", dataStr.toStdString().c_str());
    /*
     * System
     */
    if(dataStr.contains("hello"))
    {
        ROS_INFO("Update PC address");
        robotData.setPcAddress(tcpInClient->peerAddress());
        // Connect to PC
        tcpOutClient->connectToHost(tcpInClient->peerAddress(), kPCPort);
    }
    /*
     * Robot State Change
     */
    else if(dataStr.contains("autoexplore"))
    {
        if(robotData.getRobotState() != RobotState::Idle)
        {
            ROS_WARN("The robot is not idle, failed to switch to Auto Explore");
            return;
        }
        else
        {
            startAutoExplore();
        }     
    }
    else if(dataStr.contains("manualexplore"))
    {
        if(robotData.getRobotState() != RobotState::Idle)
        {
            ROS_WARN("The robot is not idle, failed to switch to Manual Explore");
            return;
        }
        else
        {
            startManualExplore();
        }     
    }
    else if(dataStr.contains("collection"))
    {
        if(robotData.getRobotState() != RobotState::Idle)
        {
            ROS_WARN("The robot is not idle, failed to switch to Collection");
            return;
        }
        else
        {
            startCollection();
        }     
    }
    else if(dataStr.contains("manualall"))
    {
        if(robotData.getRobotState() != RobotState::Idle)
        {
            ROS_WARN("The robot is not idle, failed to switch to Manual All");
            return;
        }
        else
        {
            startManualAll();
        }     
    }
    else if(dataStr.contains("manualp2p"))
    {
        if(robotData.getRobotState() != RobotState::Idle)
        {
            ROS_WARN("The robot is not idle, failed to switch to Manual P2P");
            return;
        }
        else
        {
            startManualP2P();
        }     
    }
    else if(dataStr.contains("stopall"))
    {
        if(robotData.getRobotState() == RobotState::Idle)
        {
            ROS_WARN("The robot is idle, no need to switch idle");
            return;
        }
        else
        {
            stopNodes();
        }     
    }
    else if(dataStr.contains("terminate"))
    {
        terminateThread();
    }

    /*
     * Manual
     */
    else if(dataStr.contains("manualik"))
    {
        if(robotData.getRobotState() != RobotState::ManualExplore && robotData.getRobotState() != RobotState::ManualAll)
        {
            ROS_WARN("The robot is not in Manual Mode");
            return;
        }
        else
        {
            QStringList paramList = dataStr.split(QLatin1Char(','));
            if(paramList.size() != 4)
            {
                ROS_WARN("Incorrect format for manual ik");
                return;
            }
            geometry_msgs::Twist twist;
            twist.linear.x = paramList.at(1).toFloat();
            twist.linear.y = paramList.at(2).toFloat();
            twist.angular.z = paramList.at(3).toFloat();
            manualCmdVel.publish(twist);
        }
    }
    else if(dataStr.contains("manualservo"))
    {
        if(robotData.getRobotState() != RobotState::ManualAll)
        {
            ROS_WARN("The robot is not in All Manual Mode");
            return;
        }
        else
        {
            QStringList paramList = dataStr.split(QLatin1Char(','));
            if(paramList.size() != 3)
            {
                ROS_WARN("Incorrect format for manual servo");
                return;
            }
            lgdx_robot::StmServo stmServo;
            stmServo.request.node = paramList.at(1).toInt();
            stmServo.request.angle = paramList.at(2).toInt();
            stmServoClient.call(stmServo);
        }
    }
}

void CoreSys::tcpSend(QString str)
{
    //qDebug() << "Send:"<< str;
    if(tcpOutClient->state() == QTcpSocket::ConnectedState)
    {
        str += "\n";
        tcpOutClient->write(str.toLocal8Bit());
    }
}

void CoreSys::logArrived(QString log)
{
    if(log.contains("t265") || log.contains("d400") || log.contains("rtabmap") || log.contains("lgdx_robot_auto_mode") || log.contains("move_base") || log.contains("explore"))
    {
        log = "log," + log;
        tcpSend(log);
    }
}

void CoreSys::realsenseD400Arrived()
{
    robotData.setRealsenseD400Ready(true);
}

void CoreSys::realsenseT265Arrived(int x, int y)
{
    robotData.setRealsenseT265Ready(true);
}

void CoreSys::stm32Arrived(QString data)
{
    robotData.setstm32Ready(true);
    if(++stmDelayCounter == kStmDelayTotal)
    {
        stmDelayCounter = 0;
        QString str = "stm," + data;
        tcpSend(str);
    }
}

void CoreSys::dataChanged(CoreDataType d)
{
    //qDebug() << d << "changed";
    switch (d)
    {
        case CoreDataType::RobotState:
            tcpSend(robotData.robotStateStatusStr());
            break;
        case CoreDataType::RealsenseD400Ready:
            tcpSend(robotData.d400StatusStr());
            break;
        case CoreDataType::RealsenseT265Ready:
            tcpSend(robotData.t265StatusStr());
            break;
        case CoreDataType::Stm32Ready:
            tcpSend(robotData.stm32StatusStr());
            break;
    }
}

void CoreSys::terminateThread(int unused)
{
    ROS_INFO("The system is terminating");
    stopNodes(true);
    rosThread.quit();
    ros::shutdown();
    QCoreApplication::quit();
}
