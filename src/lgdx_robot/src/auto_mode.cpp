#include <signal.h>
#include <algorithm>
#include <iostream>
#include <list>
#include <string>
#include <vector>
#include <QString>
#include <QStringList>

#include "ros/ros.h"
#include "nav_msgs/OccupancyGrid.h"
#include "nav_msgs/Path.h"
#include "nav_msgs/GetMap.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/String.h"
#include "std_srvs/Empty.h"

#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include "tf2_ros/transform_listener.h"
#include "actionlib/client/simple_action_client.h"
#include "move_base_msgs/MoveBaseAction.h"

#include "lgdx_robot/StmServo.h"
#include "move_base_msgs/MoveBaseAction.h"
#include "actionlib/client/simple_action_client.h"

/*
 * https://github.com/nobleo/full_coverage_path_planner
 */
// Type
typedef struct
{
    int x, y;
}
Point_t;

inline std::ostream &operator << (std::ostream &os, Point_t &p)
{
    return os << "(" << p.x << ", " << p.y << ")";
}

typedef struct
{
    Point_t pos;

    /** Path cost
   * cost of the path from the start node to gridNode_t
   */
    int cost;

    /** Heuristic cost
   * cost of the cheapest path from this gridNode_t to the goal
   */
    int he;
}
gridNode_t;

inline std::ostream &operator << (std::ostream &os, gridNode_t &g)
{
    return os << "gridNode_t(" << g.cost << ", " << g.he << ", " << g.pos  << ")";
}


typedef struct
{
    float x, y;
}
fPoint_t;

inline std::ostream &operator << (std::ostream &os, fPoint_t &p)
{
    return os << "(" << p.x << ", " << p.y << ")";
}

enum
{
    eNodeOpen = false,
    eNodeVisited = true
};

#ifndef dabs
#define dabs(a)     ((a) >= 0 ? (a):-(a))
#endif
#ifndef dmin
#define dmin(a, b)   ((a) <= (b) ? (a):(b))
#endif
#ifndef dmax
#define dmax(a, b)   ((a) >= (b) ? (a):(b))
#endif
#ifndef clamp
#define clamp(a, lower, upper)    dmax(dmin(a, upper), lower)
#endif

enum
{
    eDirNone = 0,
    eDirRight = 1,
    eDirUp = 2,
    eDirLeft = -1,
    eDirDown = -2,
};

float tile_size_ = 0;
fPoint_t grid_origin_;
geometry_msgs::PoseStamped previous_goal_;
ros::Publisher plan_pub_;

// common.cpp
int distanceSquared(const Point_t& p1, const Point_t& p2)
{
    int dx = p2.x - p1.x;
    int dy = p2.y - p1.y;

    int dx2 = dx * dx;
    if (dx2 != 0 && dx2 / dx != dx)
    {
        throw std::range_error("Integer overflow error for the given points");
    }

    int dy2 = dy * dy;
    if (dy2 != 0 && dy2 / dy != dy)
    {
        throw std::range_error("Integer overflow error for the given points");
    }

    if (dx2 > std::numeric_limits< int >::max() - dy2)
        throw std::range_error("Integer overflow error for the given points");
    int d2 = dx2 + dy2;

    return d2;
}

int distanceToClosestPoint(Point_t poi, std::list<Point_t> const& goals)
{
    // Return minimum distance from goals-list
    int min_dist = INT_MAX;
    std::list<Point_t>::const_iterator it;
    for (it = goals.begin(); it != goals.end(); ++it)
    {
        int cur_dist = distanceSquared((*it), poi);
        if (cur_dist < min_dist)
        {
            min_dist = cur_dist;
        }
    }
    return min_dist;
}

/**
 * Sort vector<gridNode> by the heuristic value of the last element
 * @return whether last elem. of first has a larger heuristic value than last elem of second
 */
bool sort_gridNodePath_heuristic_desc(const std::vector<gridNode_t> &first, const std::vector<gridNode_t> &second)
{
    return (first.back().he > second.back().he);
}

bool a_star_to_open_space(std::vector<std::vector<bool> > const &grid, gridNode_t init, int cost, std::vector<std::vector<bool> > &visited, std::list<Point_t> const &open_space, std::list<gridNode_t> &pathNodes)
{
    uint dx, dy, dx_prev, nRows = grid.size(), nCols = grid[0].size();

    std::vector<std::vector<bool> > closed(nRows, std::vector<bool>(nCols, eNodeOpen));
    // All nodes in the closest list are currently still open

    closed[init.pos.y][init.pos.x] = eNodeVisited;  // Of course we have visited the current/initial location
#ifdef DEBUG_PLOT
    std::cout << "A*: Marked init " << init << " as eNodeVisited (true)" << std::endl;
    printGrid(closed);
#endif

    std::vector<std::vector<gridNode_t> > open1(1, std::vector<gridNode_t>(1, init));  // open1 is a *vector* of paths

    while (true)
    {
#ifdef DEBUG_PLOT
        std::cout << "A*: open1.size() = " << open1.size() << std::endl;
#endif
        if (open1.size() == 0)  // If there are no open paths, there's no place to go and we must resign
        {
            // Empty end_node list and add init as only element
            pathNodes.erase(pathNodes.begin(), --(pathNodes.end()));
            pathNodes.push_back(init);
            return true;  // We resign, cannot find a path
        }
        else
        {
            // Sort elements from high to low (because sort_gridNodePath_heuristic_desc uses a > b)
            std::sort(open1.begin(), open1.end(), sort_gridNodePath_heuristic_desc);

            std::vector<gridNode_t> nn = open1.back();  // Get the *path* with the lowest heuristic cost
            open1.pop_back();  // The last element is no longer open because we use it here, so remove from open list
#ifdef DEBUG_PLOT
            std::cout << "A*: Check out path from" << nn.front().pos << " to " << nn.back().pos
                      << " of length " << nn.size() << std::endl;
#endif

            // Does the path nn end in open space?
            if (visited[nn.back().pos.y][nn.back().pos.x] == eNodeOpen)
            {
                // If so, we found a path to open space
                // Copy the path nn to pathNodes so we can report that path (to get to open space)
                std::vector<gridNode_t>::iterator iter;
                for (iter = nn.begin(); iter != nn.end(); ++iter)
                {
                    pathNodes.push_back((*iter));
                }

                return false;  // We do not resign, we found a path
            }
            else  // Path nn does not lead to open space
            {
                if (nn.size() > 1)
                {
                    // Create iterator for gridNode_t list and let it point to the last element of nn
                    std::vector<gridNode_t>::iterator it = --(nn.end());
                    dx = it->pos.x - (it - 1)->pos.x;
                    dy = it->pos.y - (it - 1)->pos.y;
                    // TODO(CesarLopez) docs: this seems to cycle through directions
                    // (notice the shift-by-one between both sides of the =)
                    dx_prev = dx;
                    dx = -dy;
                    dy = dx_prev;
                }
                else
                {
                    dx = 0;
                    dy = 1;
                }

                // For all nodes surrounding the end of the end of the path nn
                for (uint i = 0; i < 4; ++i)
                {
                    Point_t p2 =
                    {
                        int(nn.back().pos.x + dx),
                        int(nn.back().pos.y + dy),
                    };

#ifdef DEBUG_PLOT
                    std::cout << "A*: Look around " << i << " at p2=(" << p2 << std::endl;
#endif

                    if (p2.x >= 0 && p2.x < nCols && p2.y >= 0 && p2.y < nRows)  // Bounds check, do not sep out of map
                    {
                        // If the new node (a neighbor of the end of the path nn) is open, append it to newPath ( = nn)
                        // and add that to the open1-list of paths.
                        // Because of the pop_back on open1, what happens is that the path is temporarily 'checked out',
                        // modified here, and then added back (if the condition above and below holds)
                        if (closed[p2.y][p2.x] == eNodeOpen && grid[p2.y][p2.x] == eNodeOpen)
                        {
#ifdef DEBUG_PLOT
                            std::cout << "A*: p2=" << p2 << " is OPEN" << std::endl;
#endif
                            std::vector<gridNode_t> newPath = nn;
                            // # heuristic  has to be designed to prefer a CCW turn
                            Point_t new_point = { p2.x, p2.y };
                            gridNode_t new_node =
                            {
                                new_point,                                                                // Point: x,y
                                cost + nn.back().cost,                                                    // Cost
                                int(cost + nn.back().cost + distanceToClosestPoint(p2, open_space) + i),
                                // Heuristic (+i so CCW turns are cheaper)
                            };
                            newPath.push_back(new_node);
                            closed[new_node.pos.y][new_node.pos.x] = eNodeVisited;  // New node is now used in a path and thus visited

#ifdef DEBUG_PLOT
                            std::cout << "A*: Marked new_node " << new_node << " as eNodeVisited (true)" << std::endl;
                            std::cout << "A*: Add path from " << newPath.front().pos << " to " << newPath.back().pos
                                      << " of length " << newPath.size() << " to open1" << std::endl;
#endif
                            open1.push_back(newPath);
                        }
#ifdef DEBUG_PLOT
                        else
                        {
                            std::cout << "A*: p2=" << p2 << " is not open: "
                                                            "closed[" << p2.y << "][" << p2.x << "]=" << closed[p2.y][p2.x] << ", "
                                                                                                                               "grid["  << p2.y << "][" << p2.x << "]=" << grid[p2.y][p2.x] << std::endl;
                        }
#endif
                    }
#ifdef DEBUG_PLOT
                    else
                    {
                        std::cout << "A*: p2=(" << p2.x << ", " << p2.y << ") is out of bounds" << std::endl;
                    }
#endif
                    // Cycle around to next neighbor, CCW
                    dx_prev = dx;
                    dx = dy;
                    dy = -dx_prev;
                }
            }
        }
    }
}

std::list<Point_t> map_2_goals(std::vector<std::vector<bool> > const& grid, bool value_to_search)
{
    std::list<Point_t> goals;
    int ix, iy;
    uint nRows = grid.size();
    uint nCols = grid[0].size();
    for (iy = 0; iy < nRows; ++(iy))
    {
        for (ix = 0; ix < nCols; ++(ix))
        {
            if (grid[iy][ix] == value_to_search)
            {
                Point_t p = { ix, iy };  // x, y
                goals.push_back(p);
            }
        }
    }
    return goals;
}

// full_coverage_path_planner.cpp
void publishPlan(const std::vector<geometry_msgs::PoseStamped>& path)
{
    // create a message for the plan
    nav_msgs::Path gui_path;
    gui_path.poses.resize(path.size());

    if (!path.empty())
    {
        gui_path.header.frame_id = path[0].header.frame_id;
        gui_path.header.stamp = path[0].header.stamp;
    }

    // Extract the plan in world co-ordinates, we assume the path is all in the same frame
    for (unsigned int i = 0; i < path.size(); i++)
    {
        gui_path.poses[i] = path[i];
    }

    plan_pub_.publish(gui_path);
}

void parsePointlist2Plan(const geometry_msgs::PoseStamped& start, std::list<Point_t> const& goalpoints, std::vector<geometry_msgs::PoseStamped>& plan)
{
    geometry_msgs::PoseStamped new_goal;
    std::list<Point_t>::const_iterator it, it_next, it_prev;
    int dx_now, dy_now, dx_next, dy_next, move_dir_now = 0, move_dir_prev = 0, move_dir_next = 0;
    bool do_publish = false;
    float orientation = eDirNone;
    ROS_INFO("Received goalpoints with length: %lu", goalpoints.size());
    if (goalpoints.size() > 1)
    {
        for (it = goalpoints.begin(); it != goalpoints.end(); ++it)
        {
            it_next = it;
            it_next++;
            it_prev = it;
            it_prev--;

            // Check for the direction of movement
            if (it == goalpoints.begin())
            {
                dx_now = it_next->x - it->x;
                dy_now = it_next->y - it->y;
            }
            else
            {
                dx_now = it->x - it_prev->x;
                dy_now = it->y - it_prev->y;
                dx_next = it_next->x - it->x;
                dy_next = it_next->y - it->y;
            }

            // Calculate direction enum: dx + dy*2 will give a unique number for each of the four possible directions because
            // of their signs:
            //  1 +  0*2 =  1
            //  0 +  1*2 =  2
            // -1 +  0*2 = -1
            //  0 + -1*2 = -2
            move_dir_now = dx_now + dy_now * 2;
            move_dir_next = dx_next + dy_next * 2;

            // Check if this points needs to be published (i.e. a change of direction or first or last point in list)
            do_publish = move_dir_next != move_dir_now || it == goalpoints.begin() ||
                    (it != goalpoints.end() && it == --goalpoints.end());
            move_dir_prev = move_dir_now;

            // Add to vector if required
            if (do_publish)
            {
                new_goal.header.frame_id = "map";
                new_goal.pose.position.x = (it->x) * tile_size_ + grid_origin_.x + tile_size_ * 0.5;
                new_goal.pose.position.y = (it->y) * tile_size_ + grid_origin_.y + tile_size_ * 0.5;
                // Calculate desired orientation to be in line with movement direction
                switch (move_dir_now)
                {
                case eDirNone:
                    // Keep orientation
                    break;
                case eDirRight:
                    orientation = 0;
                    break;
                case eDirUp:
                    orientation = M_PI / 2;
                    break;
                case eDirLeft:
                    orientation = M_PI;
                    break;
                case eDirDown:
                    orientation = M_PI * 1.5;
                    break;
                }
                tf2::Quaternion myQuaternion;
                myQuaternion.setRPY(0, 0, orientation);
                new_goal.pose.orientation = tf2::toMsg(myQuaternion);
                if (it != goalpoints.begin())
                {
                    previous_goal_.pose.orientation = new_goal.pose.orientation;
                    // republish previous goal but with new orientation to indicate change of direction
                    // useful when the plan is strictly followed with base_link
                    plan.push_back(previous_goal_);
                }
                ROS_DEBUG("Voila new point: x=%f, y=%f, o=%f,%f,%f,%f", new_goal.pose.position.x, new_goal.pose.position.y,
                          new_goal.pose.orientation.x, new_goal.pose.orientation.y, new_goal.pose.orientation.z,
                          new_goal.pose.orientation.w);
                plan.push_back(new_goal);
                previous_goal_ = new_goal;
            }
        }
    }
    else
    {
        new_goal.header.frame_id = "map";
        new_goal.pose.position.x = (goalpoints.begin()->x) * tile_size_ + grid_origin_.x + tile_size_ * 0.5;
        new_goal.pose.position.y = (goalpoints.begin()->y) * tile_size_ + grid_origin_.y + tile_size_ * 0.5;
        tf2::Quaternion myQuaternion;
        myQuaternion.setRPY(0, 0, 0);
        new_goal.pose.orientation = tf2::toMsg(myQuaternion);
        plan.push_back(new_goal);
    }
    /* Add poses from current position to start of plan */

    // Compute angle between current pose and first plan point
    double dy = plan.begin()->pose.position.y - start.pose.position.y;
    double dx = plan.begin()->pose.position.x - start.pose.position.x;
    // Arbitrary choice of 100.0*FLT_EPSILON to determine minimum angle precision of 1%
    if (!(fabs(dy) < 100.0 * FLT_EPSILON && fabs(dx) < 100.0 * FLT_EPSILON))
    {
        // Add extra translation waypoint
        double yaw = std::atan2(dy, dx);
        tf2::Quaternion myQuaternion;
        myQuaternion.setRPY(0, 0, yaw);
        geometry_msgs::Quaternion quat_temp = tf2::toMsg(myQuaternion);
        geometry_msgs::PoseStamped extra_pose;
        extra_pose = *plan.begin();
        extra_pose.pose.orientation = quat_temp;
        plan.insert(plan.begin(), extra_pose);
        extra_pose = start;
        extra_pose.pose.orientation = quat_temp;
        plan.insert(plan.begin(), extra_pose);
    }

    // Insert current pose
    plan.insert(plan.begin(), start);

    ROS_INFO("Plan ready containing %lu goals!", plan.size());
}

bool parseGrid(nav_msgs::OccupancyGrid const& cpp_grid_, std::vector<std::vector<bool> >& grid, float robotRadius, float toolRadius, geometry_msgs::PoseStamped const& realStart, Point_t& scaledStart)
{
    int ix, iy, nodeRow, nodeColl;
    uint32_t nodeSize = dmax(floor(toolRadius / cpp_grid_.info.resolution), 1);  // Size of node in pixels/units
    uint32_t robotNodeSize = dmax(floor(robotRadius / cpp_grid_.info.resolution), 1);  // RobotRadius in pixels/units
    uint32_t nRows = cpp_grid_.info.height, nCols = cpp_grid_.info.width;
    ROS_INFO("nRows: %u nCols: %u nodeSize: %d", nRows, nCols, nodeSize);

    if (nRows == 0 || nCols == 0)
    {
        return false;
    }

    // Save map origin and scaling
    tile_size_ = nodeSize * cpp_grid_.info.resolution;  // Size of a tile in meters
    grid_origin_.x = cpp_grid_.info.origin.position.x;  // x-origin in meters
    grid_origin_.y = cpp_grid_.info.origin.position.y;  // y-origin in meters

    // Scale starting point
    scaledStart.x = static_cast<unsigned int>(clamp((realStart.pose.position.x - grid_origin_.x) / tile_size_, 0.0,
                                                    floor(cpp_grid_.info.width / tile_size_)));
    scaledStart.y = static_cast<unsigned int>(clamp((realStart.pose.position.y - grid_origin_.y) / tile_size_, 0.0,
                                                    floor(cpp_grid_.info.height / tile_size_)));

    // Scale grid
    for (iy = 0; iy < nRows; iy = iy + nodeSize)
    {
        std::vector<bool> gridRow;
        for (ix = 0; ix < nCols; ix = ix + nodeSize)
        {
            bool nodeOccupied = false;
            for (nodeRow = 0; (nodeRow < robotNodeSize) && ((iy + nodeRow) < nRows) && (nodeOccupied == false); ++nodeRow)
            {
                for (nodeColl = 0; (nodeColl < robotNodeSize) && ((ix + nodeColl) < nCols); ++nodeColl)
                {
                    int index_grid = dmax((iy + nodeRow - ceil(static_cast<float>(robotNodeSize - nodeSize) / 2.0))
                                          * nCols + (ix + nodeColl - ceil(static_cast<float>(robotNodeSize - nodeSize) / 2.0)), 0);
                    if (cpp_grid_.data[index_grid] > 65)
                    {
                        nodeOccupied = true;
                        break;
                    }
                }
            }
            gridRow.push_back(nodeOccupied);
        }
        grid.push_back(gridRow);
    }
    return true;
}

// spiral_stc.cpp
std::list<gridNode_t> spiral(std::vector<std::vector<bool> > const& grid, std::list<gridNode_t>& init, std::vector<std::vector<bool> >& visited)
{
    int dx, dy, dx_prev, x2, y2, i, nRows = grid.size(), nCols = grid[0].size();
    // Spiral filling of the open space
    // Copy incoming list to 'end'
    std::list<gridNode_t> pathNodes(init);
    // Create iterator for gridNode_t list and let it point to the last element of end
    std::list<gridNode_t>::iterator it = --(pathNodes.end());
    if (pathNodes.size() > 1)  // if list is length 1, keep iterator at end
        it--;                    // Let iterator point to second to last element

    gridNode_t prev = *(it);
    bool done = false;
    while (!done)
    {
        if (it != pathNodes.begin())
        {
            // turn ccw
            dx = pathNodes.back().pos.x - prev.pos.x;
            dy = pathNodes.back().pos.y - prev.pos.y;
            dx_prev = dx;
            dx = -dy;
            dy = dx_prev;
        }
        else
        {
            // Initialize spiral direction towards y-axis
            dx = 0;
            dy = 1;
        }
        done = true;

        for (int i = 0; i < 4; ++i)
        {
            x2 = pathNodes.back().pos.x + dx;
            y2 = pathNodes.back().pos.y + dy;
            if (x2 >= 0 && x2 < nCols && y2 >= 0 && y2 < nRows)
            {
                if (grid[y2][x2] == eNodeOpen && visited[y2][x2] == eNodeOpen)
                {
                    Point_t new_point = { x2, y2 };
                    gridNode_t new_node =
                    {
                        new_point,  // Point: x,y
                        0,          // Cost
                        0,          // Heuristic
                    };
                    prev = pathNodes.back();
                    pathNodes.push_back(new_node);
                    it = --(pathNodes.end());
                    visited[y2][x2] = eNodeVisited;  // Close node
                    done = false;
                    break;
                }
            }
            // try next direction cw
            dx_prev = dx;
            dx = dy;
            dy = -dx_prev;
        }
    }
    return pathNodes;
}

std::list<Point_t> spiral_stc(std::vector<std::vector<bool> > const& grid, Point_t& init, int &multiple_pass_counter, int &visited_counter)
{
    int x, y, nRows = grid.size(), nCols = grid[0].size();
    // Initial node is initially set as visited so it does not count
    multiple_pass_counter = 0;
    visited_counter = 0;

    std::vector<std::vector<bool> > visited;
    visited = grid;  // Copy grid matrix
    x = init.x;
    y = init.y;

    Point_t new_point = { x, y };
    gridNode_t new_node =
    {
        new_point,  // Point: x,y
        0,          // Cost
        0,          // Heuristic
    };
    std::list<gridNode_t> pathNodes;
    std::list<Point_t> fullPath;
    pathNodes.push_back(new_node);
    visited[y][x] = eNodeVisited;

#ifdef DEBUG_PLOT
    ROS_INFO("Grid before walking is: ");
    printGrid(grid, visited, fullPath);
#endif

    pathNodes = spiral(grid, pathNodes, visited);                // First spiral fill
    std::list<Point_t> goals = map_2_goals(visited, eNodeOpen);  // Retrieve remaining goalpoints
    // Add points to full path
    std::list<gridNode_t>::iterator it;
    for (it = pathNodes.begin(); it != pathNodes.end(); ++it)
    {
        Point_t newPoint = { it->pos.x, it->pos.y };
        visited_counter++;
        fullPath.push_back(newPoint);
    }
    // Remove all elements from pathNodes list except last element
    pathNodes.erase(pathNodes.begin(), --(pathNodes.end()));

#ifdef DEBUG_PLOT
    ROS_INFO("Current grid after first spiral is");
    printGrid(grid, visited, fullPath);
    ROS_INFO("There are %d goals remaining", goals.size());
#endif
    while (goals.size() != 0)
    {
        // Remove all elements from pathNodes list except last element.
        // The last point is the starting point for a new search and A* extends the path from there on
        pathNodes.erase(pathNodes.begin(), --(pathNodes.end()));
        visited_counter--;  // First point is already counted as visited
        // Plan to closest open Node using A*
        // `goals` is essentially the map, so we use `goals` to determine the distance from the end of a potential path
        //    to the nearest free space
        bool resign = a_star_to_open_space(grid, pathNodes.back(), 1, visited, goals, pathNodes);
        if (resign)
        {
#ifdef DEBUG_PLOT
            ROS_INFO("A_star_to_open_space is resigning", goals.size());
#endif
            break;
        }

        // Update visited grid
        for (it = pathNodes.begin(); it != pathNodes.end(); ++it)
        {
            if (visited[it->pos.y][it->pos.x])
            {
                multiple_pass_counter++;
            }
            visited[it->pos.y][it->pos.x] = eNodeVisited;
        }
        if (pathNodes.size() > 0)
        {
            multiple_pass_counter--;  // First point is already counted as visited
        }

#ifdef DEBUG_PLOT
        ROS_INFO("Grid with path marked as visited is:");
        gridNode_t SpiralStart = pathNodes.back();
        printGrid(grid, visited, pathNodes, pathNodes.front(), pathNodes.back());
#endif

        // Spiral fill from current position
        pathNodes = spiral(grid, pathNodes, visited);

#ifdef DEBUG_PLOT
        ROS_INFO("Visited grid updated after spiral:");
        printGrid(grid, visited, pathNodes, SpiralStart, pathNodes.back());
#endif

        goals = map_2_goals(visited, eNodeOpen);  // Retrieve remaining goalpoints

        for (it = pathNodes.begin(); it != pathNodes.end(); ++it)
        {
            Point_t newPoint = { it->pos.x, it->pos.y };
            visited_counter++;
            fullPath.push_back(newPoint);
        }
    }

    return fullPath;
}

typedef actionlib::SimpleActionClient<move_base_msgs::MoveBaseAction> MoveBaseClient;

// Service
ros::ServiceClient stopRtabmap;

// Goal Manage
std::vector<geometry_msgs::PoseStamped> plan;
bool haveGoal = false;
bool collecting = false;
bool allowClose = false;

// Collection
double lastYMove = 0;
int usingBin = -1;
int laserValue[3] = {0};

MoveBaseClient *ac;
ros::ServiceClient stmServoClient;

/*
 * Handle the SIGINT (CTRL+C)
 */
void sigintHandler(int sig)
{
    if(ac != nullptr)
        delete ac;
    ROS_INFO("System is terminating");
    ros::shutdown();
    exit(0);
}

void recoverGoalFinished(const actionlib::SimpleClientGoalState& status, const move_base_msgs::MoveBaseResultConstPtr&)
{
    ROS_DEBUG("Reached goal with status: %s", status.toString().c_str());
    if (status == actionlib::SimpleClientGoalState::ABORTED || status == actionlib::SimpleClientGoalState::PREEMPTED)
    {
        ROS_INFO("Recover Goal failed to reach");
        haveGoal = false;
        collecting = false;
        return;
    }
    // Succeed
    ROS_INFO("Collection end");
    haveGoal = false;
    collecting = false;
}

void collectGoalFinished(const actionlib::SimpleClientGoalState& status, const move_base_msgs::MoveBaseResultConstPtr&)
{
    ROS_DEBUG("Reached \"collection\" goal with status: %s", status.toString().c_str());
    /*
    if (status == actionlib::SimpleClientGoalState::ABORTED || status == actionlib::SimpleClientGoalState::PREEMPTED)
    {
        ROS_INFO("Collection Goal failed to reach");
        haveGoal = false;
        collecting = false;
        // Close Door
        lgdx_robot::StmServo stmServo;
        stmServo.request.node = 15;
        stmServo.request.angle = 0;
        stmServoClient.call(stmServo);
        // bin down
        stmServo.request.node = (5 + usingBin);
        stmServo.request.angle = 0;
        stmServoClient.call(stmServo);
        return;
    }
    */
    // Succeed
    ROS_INFO("Goal finished, collecting rubbish");
    // Close Door
    lgdx_robot::StmServo stmServo;
    stmServo.request.node = 15;
    stmServo.request.angle = 0;
    stmServoClient.call(stmServo);
    allowClose = true;
    ros::Duration(1.0).sleep();
    // Shovel up
    stmServo.request.node = 4;
    stmServo.request.angle = 140;
    stmServoClient.call(stmServo);
    ros::Duration(1.0).sleep();
    // Shovel down
    stmServo.request.node = 4;
    stmServo.request.angle = 0;
    stmServoClient.call(stmServo);
    ros::Duration(0.2).sleep();
    
    ROS_INFO("Collection end");
    haveGoal = false;
    collecting = false;
    // Recover
    /*
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose.header.frame_id = "base_link";
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.y = lastYMove;
    goal.target_pose.pose.orientation.w = 1.0;
    ac->sendGoal(
                goal, [](const actionlib::SimpleClientGoalState& status,
                const move_base_msgs::MoveBaseResultConstPtr& result) {
        recoverGoalFinished(status, result);
    });
    */
}

void normalGoalFinished(const actionlib::SimpleClientGoalState& status, const move_base_msgs::MoveBaseResultConstPtr&)
{
    ROS_DEBUG("Reached \"normal\" goal with status: %s", status.toString().c_str());
    if (status == actionlib::SimpleClientGoalState::ABORTED || status == actionlib::SimpleClientGoalState::PREEMPTED)
    {
        if(collecting)
        {
            ROS_INFO("Goal cancelled due to rubbish collection.");
            return;
        }
        ROS_INFO("Goal failed to reach");
        plan.erase(plan.begin());
        haveGoal = false;
        return;
    }
    // Succeed
    ROS_INFO("Goal finished");
    plan.erase(plan.begin());
    haveGoal = false;
}

void autoPlanner()
{
    publishPlan(plan);

    // Have a goal
    if(haveGoal)
        return;

    // Go to next goal
    ROS_INFO("Number of goal: %d", int(plan.size()));
    geometry_msgs::PoseStamped curPoint = plan.front();
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose.header.frame_id = curPoint.header.frame_id;
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose = curPoint.pose;
    haveGoal = true;
    ac->sendGoal(
                goal, [](const actionlib::SimpleClientGoalState& status,
                const move_base_msgs::MoveBaseResultConstPtr& result) {
        normalGoalFinished(status, result);
    });
}

void aiCallBack(const std_msgs::String::ConstPtr& msg)
{
    if(collecting)
        return;
    collecting = true;
    if(haveGoal)
        ac->cancelAllGoals();
    ros::Duration(0.5).sleep();
    ROS_INFO("Found rubbish %s", msg->data.c_str());
    QString msgStr = msg->data.c_str();
    QStringList para = msgStr.split(',');
    usingBin = para.at(0).toInt();

    // Back 10cm
    move_base_msgs::MoveBaseGoal goal;
    goal.target_pose.header.frame_id = "base_link";
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.x = -0.15;
    goal.target_pose.pose.position.y = 0;
    goal.target_pose.pose.position.z = 0;
    goal.target_pose.pose.orientation.w = 1.0;
    ac->sendGoal(goal);
    ac->waitForResult();
    ros::Duration(0.5).sleep();
    // Open Bin
    lgdx_robot::StmServo stmServo;
    stmServo.request.node = (5 + usingBin);
    stmServo.request.angle = 90;
    stmServoClient.call(stmServo);
    ros::Duration(0.2).sleep();
    // Open Door
    stmServo.request.node = 15;
    stmServo.request.angle = 180;
    stmServoClient.call(stmServo);
    ros::Duration(0.2).sleep();

    // Go to rubbish
    goal.target_pose.header.frame_id = "base_link";
    goal.target_pose.header.stamp = ros::Time::now();
    goal.target_pose.pose.position.x = (para.at(1).toDouble()/100) + 0.1;
    goal.target_pose.pose.position.y = (para.at(2).toDouble()/100);
    goal.target_pose.pose.position.z = 0;
    goal.target_pose.pose.orientation.w = 1.0;
    lastYMove = -(para.at(2).toDouble());
    haveGoal = true;
    ROS_INFO("Sending collection goal (%f, %f)", (para.at(1).toDouble()/100), (para.at(2).toDouble()/100));
    ac->sendGoal(
                goal, [](const actionlib::SimpleClientGoalState& status,
                const move_base_msgs::MoveBaseResultConstPtr& result) {
        collectGoalFinished(status, result);
    });
}

void stmCallBack(const std_msgs::String::ConstPtr& msg)
{
    //ROS_INFO("Found rubbish %s", msg->data.c_str());
    QString msgStr = msg->data.c_str();
    QStringList para = msgStr.split(',');
    int bin0 = para.at(12).toInt();
    int bin1 = para.at(13).toInt();
    int bin2 = para.at(14).toInt();
    lgdx_robot::StmServo stmServo;
    if(allowClose)
    {
        if(usingBin == 0 && bin0 < 200)
        {
            // bin down
            stmServo.request.node = (5 + usingBin);
            stmServo.request.angle = 0;
            stmServoClient.call(stmServo);
            ros::Duration(0.2).sleep();
            ROS_INFO("Close Bin 0");
            allowClose = false;
        }
        else if(usingBin == 1 && bin1 < 200)
        {
            // bin down
            stmServo.request.node = (5 + usingBin);
            stmServo.request.angle = 0;
            stmServoClient.call(stmServo);
            ros::Duration(0.2).sleep();
            ROS_INFO("Close Bin 1");
            allowClose = false;
        }
        else if(usingBin == 2 && bin2 < 200)
        {
            // bin down
            stmServo.request.node = (5 + usingBin);
            stmServo.request.angle = 0;
            stmServoClient.call(stmServo);
            ros::Duration(0.2).sleep();
            ROS_INFO("Close Bin 2");
            allowClose = false;
        }
    }

}

int main(int argc, char **argv)
{
    // Const
    float robot_radius_ = 0.29;
    float tool_radius_ = 0.29;

    // Initialize ROS
    ros::init(argc, argv, "lgdx_robot_auto_mode");
    ros::NodeHandle n("~");
    signal(SIGINT, sigintHandler);
    plan_pub_ = n.advertise<nav_msgs::Path>("plan", 1);
    ros::Subscriber sub = n.subscribe("/lgdx_robot/ai_out", 15, aiCallBack);
    ros::Subscriber sub2 = n.subscribe("/lgdx_robot/stm_out", 1000, stmCallBack);
    stmServoClient = n.serviceClient<lgdx_robot::StmServo>("/lgdx_robot/stm_servo");

    // Wait for move_base
    ac = new MoveBaseClient("move_base");
    ROS_INFO("Waiting to connect to move_base server");
    ac->waitForServer();
    ROS_INFO("Connected to move_base server");
    
    stopRtabmap = n.serviceClient<std_srvs::Empty>("/rtabmap/pause");
    std_srvs::Empty srv;
    stopRtabmap.call(srv);
    ros::Duration(3.0).sleep();
    // Get location
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);
    geometry_msgs::TransformStamped transformStamped;
    while (ros::ok())
    {
        try
        {
            transformStamped = tfBuffer.lookupTransform("map", "t265_pose_frame", ros::Time(0));
        }
        catch (tf2::TransformException &ex)
        {
            ROS_ERROR("%s", ex.what());
            ros::Duration(1.0).sleep();
            continue;
        }
        tf2::Quaternion quat;
        tf2::convert(transformStamped.transform.rotation, quat);
        double roll, pitch, yaw;
        tf2::Matrix3x3(quat).getRPY(roll, pitch, yaw);
        ROS_INFO("%f, %f, %f", transformStamped.transform.translation.x, transformStamped.transform.translation.y, yaw);
        break;
    }
    geometry_msgs::PoseStamped start;
    start.header = transformStamped.header;
    start.pose.position.x = transformStamped.transform.translation.x;
    start.pose.position.y = transformStamped.transform.translation.y;
    start.pose.position.z = 0;
    start.pose.orientation = transformStamped.transform.rotation;

    // Get Map
    Point_t startPoint;
    std::vector<std::vector<bool>> grid;
    ros::ServiceClient cpp_grid_client_ = n.serviceClient<nav_msgs::GetMap>("/rtabmap/get_map");
    nav_msgs::GetMap grid_req_srv;
    if (!cpp_grid_client_.call(grid_req_srv))
    {
        ROS_ERROR("Could not retrieve grid from map_server");
        return 1;
    }
    if (!parseGrid(grid_req_srv.response.map, grid, robot_radius_ * 2, tool_radius_ * 2, start, startPoint))
    {
        ROS_ERROR("Could not parse retrieved grid");
        return 1;
    }

    int multiple_pass_counter = 0;
    int visited_counter = 0;
    std::list<Point_t> goalPoints = spiral_stc(grid, startPoint, multiple_pass_counter, visited_counter);
    ROS_INFO("Planning finished");
    parsePointlist2Plan(start, goalPoints, plan);

    ROS_INFO("Auto mode node is running");
    ros::Timer timer = n.createTimer(ros::Duration(0.1), [](const ros::TimerEvent&) { autoPlanner(); });
    ros::spin();
    return 0;
}
