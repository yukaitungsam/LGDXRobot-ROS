# LGDXRobot-ROS

## About

A higl level controller software using ROS, it has follwoing functions.

* ROS node management
* Communication (MCU & Remote)
* Detecting rubbish with AI

## Hardware Requirement

This software is developed for the specific robot, it has

* MCU with [firmware](https://gitlab.com/yukaitungsam/LGDXRobot-Remote) installed
* Nvidia Jetson Nano (Other Jetson should be fine)
* Intel Realsense D435 and T265

## Software Dependency

### Intel Realsense SDK

Refer the link below to install 

[https://github.com/JetsonHacksNano/installLibrealsense](https://github.com/JetsonHacksNano/installLibrealsense)

### Jetson-inference

Refer the link below to install

[https://github.com/dusty-nv/jetson-inference](https://github.com/dusty-nv/jetson-inference)

### ROS & ROS Packages

Currently using ROS Melodic in the system, follow the standard instruction to install ROS melodic, then install packages below.

```bash
# Intel Realsense for ROS
sudo apt install ros-melodic-realsense2-camera
# Rtabmap: SLAM and localization
sudo apt install ros-melodic-rtabmap-ros
# ROS Navigation
sudo apt install ros-melodic-move-base
sudo apt install ros-melodic-dwa-local-planner
sudo apt install ros-melodic-global-planner
# Visualization 
sudo apt install ros-melodic-web-video-server
sudo apt install ros-melodic-tf2-web-republisher
sudo apt install ros-melodic-rosbridge-server
```

### Qt

```bash
sudo apt install qt5-default libqt5serialport5
```

## Usage

```bash
sudo usermod -a -G dialout $USER
```

Unzip the folder to home directory and run the following command.

```bash
source ~/<folder name>/devel/setup.bash
roscore
rosrun lgdx_robot core
```

## Credit

This repository includes algorthims below.

* [Explore Lite](https://github.com/hrnr/m-explore)
* [Full Coverage Path Planner](http://wiki.ros.org/full_coverage_path_planner)

## Reference Image

![Image](img.jpg)
